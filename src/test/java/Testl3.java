import com.noise.Polynom;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class Testl3 {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @BeforeClass
    public static void start() {
        System.out.println("Запуск тестов...");
    }
    @Test
    public void test1(){
        Assert.assertEquals(0.6944444444444445,
                Polynom.polySum(new String[] {"1", "2", "3", "4", "5"}) ,0 );
    }
    @Test
    public void test2(){
        Assert.assertEquals(0.7611111111111112,
                Polynom.polySum(new String[] {"1", "a", "b", "c", "5", "6"}) ,0 );
    }
    @Test
    public void test3(){
        Assert.assertEquals(0.3333333333333333,
                Polynom.polySum(new String[] {"33", "1000"}) ,0 );
    }
    @Test
    public void test4(){
        Assert.assertEquals(0.5,
                Polynom.polySum(new String[] {"-1","-3","-6"}) ,0 );
    }
    @Test
    public void test5(){
        Assert.assertEquals(0.8166666666666668,
                Polynom.polySum(new String[] {"=","*","-","+",",","0.7","0.0009"}) ,0 );
    }
   @Test
   public void testnull() {
       Exception exception = assertThrows(Exception.class, ()->{ Polynom.polySum(null);});
       Assert.assertTrue(exception instanceof NullPointerException);
   }
    @AfterClass
    public static void finish() {
        System.out.println("Тесты Завершены...");
    }
}
