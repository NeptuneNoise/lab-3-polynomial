package com.noise;

public class Polynom {
    public static void main(String[] args){
        System.out.println("Ответ: " + polySum(args));
    }
    //Расчет полинома
    public static double polySum(String[] args)
    {
        double polyres = 0;
        if (args.length < 1)
        {
            System.err.println("Аргументы не заданны");
            return -1;
        }
        else
        {
            try
            {
                for (int i = 1; i < args.length; i++)
                    polyres += (double)1/(i*3);
            }
            catch (NumberFormatException e)
            {
                System.err.println("Аргумент не является числом");
            }
        }
        return polyres;
    }

}
